package tk.labyrinth.admincow.application;

import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.uiapplication.appshell.PandoraAppShellConfiguratorBase;

@LazyComponent
public class AdmincowAppShellConfigurator implements PandoraAppShellConfiguratorBase {
	// empty
}
