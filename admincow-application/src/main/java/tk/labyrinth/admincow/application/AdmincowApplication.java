package tk.labyrinth.admincow.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import tk.labyrinth.pandora.iam.PandoraIamModule;
import tk.labyrinth.pandora.ui.PandoraUiModule;

@Import({
		PandoraIamModule.class,
		PandoraUiModule.class,
})
@SpringBootApplication
public class AdmincowApplication {

	public static void main(String... args) {
		SpringApplication.run(AdmincowApplication.class, args);
	}
}
