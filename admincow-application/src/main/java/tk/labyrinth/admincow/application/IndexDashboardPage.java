package tk.labyrinth.admincow.application;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;

@RequiredArgsConstructor
//@Route(value = "", layout = ConfigurableAppLayout.class)
public class IndexDashboardPage extends CssVerticalLayout {

	@PostConstruct
	private void postConstruct() {
		add("INDEX-DASHBOARD-PAGE");
	}
}
