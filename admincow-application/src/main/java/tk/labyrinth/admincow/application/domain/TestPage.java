package tk.labyrinth.admincow.application.domain;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.router.Route;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectReference;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.stores.domain.mongodb.MongoDbDatatypeConstants;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectSearcher;
import tk.labyrinth.pandora.views.style.PandoraStyles;

import java.util.stream.IntStream;

@RequiredArgsConstructor
@Route("te-test")
public class TestPage extends CssVerticalLayout {

	private final GenericObjectSearcher genericObjectSearcher;

	@PostConstruct
	private void postConstruct() {
		addClassName(PandoraStyles.LAYOUT);
		//
		add(ButtonRenderer.render(builder -> builder
				.text("Run")
				.onClick(event -> {
					getChildren().skip(1).forEach(Component::removeFromParent);
					//
					IntStream.range(0, 1).forEach(index -> {
						long start = System.currentTimeMillis();
						genericObjectSearcher.resolve(GenericObjectReference.from(
								JavaBaseTypeUtils.createDatatypeBaseReference(ClassSignature.class).toGenericReference()));
						long end = System.currentTimeMillis();
						add(new Span("CLSG %s: %sms".formatted(index, (end - start))));
					});
					//
					IntStream.range(0, 1).forEach(index -> {
						long start = System.currentTimeMillis();
						genericObjectSearcher.resolve(GenericObjectReference.from(
								AliasDatatypeBaseReference.of(MongoDbDatatypeConstants.OBJECT_ID_ALIAS).toGenericReference()));
						long end = System.currentTimeMillis();
						add(new Span("MDOI %s: %sms".formatted(index, (end - start))));
					});
				})
				.build()));
	}
}
