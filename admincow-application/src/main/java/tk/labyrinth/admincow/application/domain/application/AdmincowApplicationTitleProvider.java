package tk.labyrinth.admincow.application.domain.application;

import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.uiapplication.navbar.PandoraApplicationTitleProvider;

@Bean
public class AdmincowApplicationTitleProvider implements PandoraApplicationTitleProvider {

	@Override
	public String provideApplicationTitle() {
		return "AdminCow";
	}
}
