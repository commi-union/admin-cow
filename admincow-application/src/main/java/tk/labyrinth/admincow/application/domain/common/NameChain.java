package tk.labyrinth.admincow.application.domain.common;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class NameChain {

	@NonNull
	List<String> segments;

	public NameChain append(String name) {
		return new NameChain(segments.append(name));
	}

	public String getLast() {
		return segments.last();
	}

	@Override
	public String toString() {
		return segments.mkString(".");
	}

	public static NameChain of(String name) {
		return new NameChain(List.of(name));
	}
}
