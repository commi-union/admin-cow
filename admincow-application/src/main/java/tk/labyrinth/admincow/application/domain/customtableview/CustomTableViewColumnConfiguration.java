package tk.labyrinth.admincow.application.domain.customtableview;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.render.RenderPattern;

@Builder(builderClassName = "Builder", toBuilder = true)
@RenderPattern("$attributeName (v:$visible)")
@Value
@With
public class CustomTableViewColumnConfiguration {

	String attributeName;

	String displayName;

	Integer flexGrow;

	Boolean visible;
}
