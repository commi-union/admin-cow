package tk.labyrinth.admincow.application.domain.customtableview;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.root.RootModel;
import tk.labyrinth.pandora.stores.rootobject.HasUid;

import java.util.UUID;

@Builder(builderClassName = "Builder", toBuilder = true)
@RootModel(code = CustomTableViewConfiguration.MODEL_CODE, primaryAttribute = HasUid.UID_ATTRIBUTE_NAME)
@Value
@With
public class CustomTableViewConfiguration implements HasUid {

	public static final String MODEL_CODE = "customtableviewconfiguration";

	List<CustomTableViewColumnConfiguration> columns;

	String name;

	CodeObjectModelReference objectModelReference;

	UUID uid;

	public List<CustomTableViewColumnConfiguration> getColumnsOrEmpty() {
		return columns != null ? columns : List.empty();
	}
}
