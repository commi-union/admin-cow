package tk.labyrinth.admincow.application.domain.customtableview;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.functionalvaadin.component.CheckboxRenderer;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.ui.domain.objectview.RegisterObjectViewRenderer;
import tk.labyrinth.pandora.ui.domain.objectview.TypedObjectViewRenderer;
import tk.labyrinth.pandora.views.box.BoxRendererRegistry;
import tk.labyrinth.pandora.views.box.IntegerBoxRenderer;
import tk.labyrinth.pandora.views.style.PandoraStyles;

@Bean
@RegisterObjectViewRenderer("Main")
@RequiredArgsConstructor
public class CustomTableViewConfigurationViewRenderer implements TypedObjectViewRenderer<CustomTableViewConfiguration> {

	private final BoxRendererRegistry boxRendererRegistry;

	@SmartAutowired
	private TypedObjectSearcher<ObjectModel> objectModelSearcher;

	@Override
	public Component render(Parameters<CustomTableViewConfiguration> parameters) {
		CssGridLayout layout = new CssGridLayout();
		{
			layout.addClassName(PandoraStyles.LAYOUT);
			//
			layout.setGridTemplateAreas(
					"name objectModelReference",
					"columns columns");
			//
			layout.setWidth("40em");
		}
		{
			layout.add(
					boxRendererRegistry.getRenderer(String.class)
							.render(builder -> builder
									.currentValue(parameters.currentValue().getName())
									.initialValue(parameters.currentValue().getName())
									.label("Name")
									.onValueChange(nextValue -> parameters.onValueChange().accept(
											parameters.currentValue().withName(nextValue)))
									.build())
							.asVaadinComponent(),
					"name"
			);
			//
			layout.add(
					boxRendererRegistry.getRenderer(CodeObjectModelReference.class)
							.render(builder -> builder
									.currentValue(parameters.currentValue().getObjectModelReference())
									.initialValue(parameters.initialValue().getObjectModelReference())
									.label("Object Model Reference")
									.onValueChange(nextValue -> parameters.onValueChange().accept(
											parameters.currentValue().toBuilder()
													.columns(nextValue != null
															? objectModelSearcher.getSingle(nextValue).getAttributes()
															.map(attribute -> CustomTableViewColumnConfiguration.builder()
																	.attributeName(attribute.getName())
																	.visible(true)
																	.build())
															: null)
													.objectModelReference(nextValue)
													.build()))
									.build())
							.asVaadinComponent(),
					"objectModelReference");
			//
			{
				List<CustomTableViewColumnConfiguration> columns = parameters.currentValue().getColumnsOrEmpty();
				//
				Grid<CustomTableViewColumnConfiguration> grid = new Grid<>();
				{
					grid.setSelectionMode(Grid.SelectionMode.NONE);
					//
					grid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
				}
				{
					grid
							.addColumn(CustomTableViewColumnConfiguration::getAttributeName)
							.setHeader("Attribute Name")
							.setResizable(true);
					grid
							.addComponentColumn(item -> CheckboxRenderer.render(builder -> builder
									.onValueChange(nextValue -> {
										int index = columns.indexOf(item);
										//
										parameters.onValueChange().accept(parameters.currentValue().withColumns(
												columns.update(index, item.withVisible(!item.getVisible()))));
									})
									.value(item.getVisible())
									.build()))
							.setFlexGrow(0)
							.setHeader("Visible")
							.setResizable(true);
					grid
							.addComponentColumn(item -> IntegerBoxRenderer
									.render(builder -> builder
											.currentValue(item.getFlexGrow())
											.initialValue(item.getFlexGrow())
											.min(0)
											.onValueChange(nextValue -> {
												int index = columns.indexOf(item);
												//
												parameters.onValueChange().accept(parameters.currentValue().withColumns(
														columns.update(index, item.withFlexGrow(nextValue))));
											})
											.stepButtonsVisible(true)
											.width("6em")
											.build())
									.asVaadinComponent())
							.setHeader("Flex Grow")
							.setResizable(true);
				}
				{
					grid.setItems(columns.asJava());
				}
				layout.add(grid, "columns");
			}
		}
		return layout;
	}
}
