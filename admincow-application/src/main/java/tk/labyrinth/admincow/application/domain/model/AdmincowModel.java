package tk.labyrinth.admincow.application.domain.model;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.admincow.application.domain.store.NameAdmincowStoreConfigurationReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.render.RenderPattern;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.root.RootModel;
import tk.labyrinth.pandora.stores.rootobject.HasUid;

import java.util.UUID;

@Builder(builderClassName = "Builder", toBuilder = true)
@RootModel(code = AdmincowModel.MODEL_CODE, primaryAttribute = HasUid.UID_ATTRIBUTE_NAME)
@RenderPattern("${storeConfigurationReference.name}:$collectionName")
@Value
@With
public class AdmincowModel implements HasUid {

	public static final String MODEL_CODE = "admincowmodel";

	String collectionName;

	List<ObjectModel> objectModels;

	NameAdmincowStoreConfigurationReference storeConfigurationReference;

	UUID uid;
}
