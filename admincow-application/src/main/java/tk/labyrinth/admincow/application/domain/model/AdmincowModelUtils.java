package tk.labyrinth.admincow.application.domain.model;

import tk.labyrinth.admincow.application.domain.common.NameChain;
import tk.labyrinth.admincow.application.domain.store.AdmincowStoreConfiguration;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;

public class AdmincowModelUtils {

	public static String composeObjectModelCode(String objectModelCodePrefix, NameChain nameChain) {
		return "%s%s".formatted(objectModelCodePrefix, nameChain.toString().toLowerCase());
	}

	public static String computeRootObjectModelCode(AdmincowModel admincowModel) {
		return "admincow:%s:%s".formatted(
				admincowModel.getStoreConfigurationReference().getName(),
				admincowModel.getObjectModels().get(0).getCode());
	}

	public static Predicate computeRootObjectModelReferencePredicate(
			AdmincowStoreConfiguration admincowStoreConfiguration) {
		return Predicates.startsWith(
				"modelReference",
				"objectmodel(code=admincow:%s:".formatted(admincowStoreConfiguration.getName()),
				true);
	}
}
