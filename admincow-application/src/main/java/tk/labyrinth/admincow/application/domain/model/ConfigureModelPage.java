package tk.labyrinth.admincow.application.domain.model;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.Route;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalPage;
import tk.labyrinth.pandora.stores.rootobject.UidReference;
import tk.labyrinth.pandora.ui.domain.applayout.ConfigurableAppLayout;
import tk.labyrinth.pandora.ui.domain.state.model.UiStateAttribute;

import java.util.UUID;
import java.util.function.Consumer;

@RequiredArgsConstructor
@Route(value = "configure-model", layout = ConfigurableAppLayout.class)
public class ConfigureModelPage extends FunctionalPage<ConfigureModelPage.State> {

	private final ConfigureModelViewRenderer configureModelViewRenderer;

	@Override
	protected State getInitialProperties() {
		return State.builder()
				.exploreString(null)
				.valueUid(null)
				.build();
	}

	@Override
	protected Component render(State state, Consumer<State> sink) {
		return configureModelViewRenderer.render(builder -> builder
				.exploreString(state.exploreString())
				.onValueReferenceChange(nextValueReference -> sink.accept(state.toBuilder()
						.exploreString(null)
						.valueUid(nextValueReference != null ? nextValueReference.getUid() : null)
						.build()))
				.valueReference(state.valueUid() != null
						? UidReference.of(AdmincowModel.MODEL_CODE, state.valueUid())
						: null)
				.build());
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class State {

		/**
		 * ${storeConfiguration.name}:$collectionName
		 */
		@Nullable
		@UiStateAttribute(queryAttributeName = "explore")
		String exploreString;
		//
//		@Nullable
//		UidReference<AdmincowModel> valueReference;

		@Nullable
		@UiStateAttribute(queryAttributeName = "uid")
		UUID valueUid;
	}
}
