package tk.labyrinth.admincow.application.domain.model;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttribute;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ButtonRenderer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.ui.renderer.tovaadincomponent.ToVaadinComponentRendererRegistry;

@LazyComponent
@RequiredArgsConstructor
public class ConfigureModelTool {

	private final ToVaadinComponentRendererRegistry toVaadinComponentRendererRegistry;

	public Component renderObjectModelAttributesGrid(List<ObjectModelAttribute> objectModelAttributes) {
		Grid<ObjectModelAttribute> grid = new Grid<>();
		{
			grid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
			//
			grid.setAllRowsVisible(true);
			grid.setSelectionMode(Grid.SelectionMode.NONE);
		}
		{
			grid
					.addColumn(ObjectModelAttribute::getName)
					.setHeader("Name")
					.setResizable(true);
			grid
					.addComponentColumn(item -> toVaadinComponentRendererRegistry.render(
							ToVaadinComponentRendererRegistry.Context.builder()
									.datatype(Datatype.class)
									.hints(List.empty())
									.build(),
							item.getDatatype()))
					.setHeader("Datatype")
					.setResizable(true);
		}
		{
			grid.setItems(objectModelAttributes.asJava());
		}
		return grid;
	}

	public Component renderObjectModelsGrid(List<ObjectModel> objectModels) {
		Grid<ObjectModel> grid = new Grid<>();
		{
			grid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
			//
			grid.setAllRowsVisible(true);
			grid.setSelectionMode(Grid.SelectionMode.NONE);
		}
		{
			grid
					.addColumn(ObjectModel::getCode)
					.setHeader("Code")
					.setResizable(true);
			grid
					.addColumn(ObjectModel::getName)
					.setHeader("Name")
					.setResizable(true);
			grid
					.addColumn(item -> item.getAttributes().map(ObjectModelAttribute::getName).mkString(", "))
					.setFlexGrow(3)
					.setHeader("Attributes")
					.setResizable(true);
			grid
					.addComponentColumn(item -> ButtonRenderer.render(builder -> builder
							.enabled(false)
							.icon(VaadinIcon.EDIT.create())
							.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY)
							.build()))
					.setFlexGrow(0)
					.setHeader("Edit (TODO)")
					.setResizable(true);
		}
		{
			grid.setDetailsVisibleOnClick(false);
			grid.addItemClickListener(event ->
					grid.setDetailsVisible(event.getItem(), !grid.isDetailsVisible(event.getItem())));
			grid.setItemDetailsRenderer(new ComponentRenderer<>(item -> renderObjectModelAttributesGrid(
					item.getAttributes())));
		}
		{
			grid.setItems(objectModels.asJava());
			//
			objectModels.forEach(objectModel -> grid.setDetailsVisible(objectModel, true));
		}
		return grid;
	}
}
