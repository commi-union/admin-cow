package tk.labyrinth.admincow.application.domain.model;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import lombok.val;
import org.apache.commons.lang3.reflect.TypeUtils;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.admincow.application.domain.mongodb.MongoDbUtils;
import tk.labyrinth.admincow.application.domain.store.AdmincowStoreConfiguration;
import tk.labyrinth.admincow.application.domain.store.AdmincowStoreUtils;
import tk.labyrinth.admincow.application.domain.store.CollectionDescription;
import tk.labyrinth.admincow.application.domain.store.NameAdmincowStoreConfigurationReference;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationViews;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.pattern.PandoraPattern;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.stores.rootobject.UidReference;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxRegistry;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class ConfigureModelViewRenderer {

	private final ConfigureModelTool configureModelTool;

	private final ValueBoxRegistry valueBoxRegistry;

	@SmartAutowired
	private TypedObjectManipulator<AdmincowModel> modelManipulator;

	@SmartAutowired
	private TypedObjectSearcher<AdmincowModel> modelSearcher;

	@SmartAutowired
	private TypedObjectSearcher<AdmincowStoreConfiguration> storeConfigurationSearcher;

	private Component render(State state) {
		Parameters parameters = state.parameters();
		AdmincowModel currentValue = state.currentValue();
		//
		CssGridLayout layout = new CssGridLayout();
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
			//
			layout.setAlignItems(AlignItems.BASELINE);
			//
			layout.setGridTemplateAreas(
					"selector name save delete . .",
					"model model model model model model");
			layout.setGridTemplateColumns("repeat(1fr,6)");
			layout.setGridTemplateRows("auto 1fr");
		}
		{
			{
				{
					MutableValueBox<UidReference<AdmincowModel>> nameBox = valueBoxRegistry.getValueBoxInferred(
							TypeUtils.parameterize(UidReference.class, AdmincowModel.class));
					//
					nameBox.render(builder -> builder
							.currentValue(parameters.valueReference())
							.initialValue(parameters.valueReference())
							.label("Model")
							.onValueChange(state.parameters().onValueReferenceChange())
							.build());
					//
					layout.add(nameBox.asVaadinComponent(), "selector");
				}
				{
					MutableValueBox<String> nameBox = valueBoxRegistry.getValueBox(String.class);
					//
					nameBox.render(builder -> builder
							.currentValue(!currentValue.getObjectModels().isEmpty() ?
									currentValue.getObjectModels().get(0).getName()
									: null)
							.initialValue(Optional.ofNullable(state.initialValue().getObjectModels())
									.map(objectModels -> !objectModels.isEmpty() ? objectModels.get(0).getName() : null)
									.orElse(null))
							.label("Name")
							.onValueChange(nextValue -> {
								// FIXME: Does it work? If yes, how? If no, need to change?
//								state.withCurrentValue(
//										currentValue.withObjectModels(
//												currentValue.getObjectModels().replace(
//														currentValue.getObjectModels().get(0),
//														currentValue.getObjectModels().get(0).withName(
//																nextValue))));
							})
							.build());
					//
					layout.add(nameBox.asVaadinComponent(), "name");
				}
				{
					layout.add(
							ButtonRenderer.render(builder -> builder
									.disableOnClick(true)
									.enabled(!Objects.equals(currentValue, state.initialValue()))
									.onClick(event -> {
										AdmincowModel modelToSave = currentValue.getUid() != null
												? currentValue
												: currentValue.withUid(UUID.randomUUID());
										//
										modelManipulator.createOrUpdate(modelToSave);
										//
										state.parameters().onValueReferenceChange().accept(
												UidReference.of(AdmincowModel.MODEL_CODE, modelToSave.getUid()));
									})
									.text("Save")
									.themeVariants(ButtonVariant.LUMO_PRIMARY)
									.build()),
							"save");
				}
				{
					layout.add(
							ButtonRenderer.render(builder -> builder
									.disableOnClick(true)
									.enabled(state.initialValue().getUid() != null)
									.onClick(event -> ConfirmationViews.showTextDialog("Delete?")
											.subscribeAlwaysAccepted(success -> {
												modelManipulator.delete(
														UidReference.of(AdmincowModel.MODEL_CODE, state.initialValue().getUid()));
												//
												state.parameters().onValueReferenceChange().accept(null);
											}))
									.icon(VaadinIcon.TRASH.create())
//									.themeVariants(ButtonVariant.LUMO_PRIMARY)
									.build()),
							"delete");
				}
			}
			{
				layout.add(
						configureModelTool.renderObjectModelsGrid(currentValue.getObjectModels()),
						"model");
			}
		}
		return layout;
	}

	public AdmincowModel findOrCreateModel(
			@Nullable UidReference<AdmincowModel> reference,
			@Nullable String exploreString) {
		AdmincowModel result;
		{
			if (reference != null) {
				result = modelSearcher.getSingle(reference);
				//
			} else if (exploreString != null) {
				Map<String, String> parsedExploreString = PandoraPattern.parse(
						"$storeConfigurationName:$collectionName",
						exploreString);
				//
				val storeConfigurationReference = NameAdmincowStoreConfigurationReference.of(
						parsedExploreString.get("storeConfigurationName").get());
				//
				AdmincowStoreConfiguration storeConfiguration = storeConfigurationSearcher.getSingle(
						storeConfigurationReference);
				//
				CollectionDescription collectionDescription = MongoDbUtils.exploreMongoCollection(
						storeConfiguration.getUrl(),
						parsedExploreString.get("collectionName").get(),
						AdmincowStoreUtils.composeObjectModelCodePrefix(storeConfiguration));
				//
				result = collectionDescription.getModel().toBuilder()
						.storeConfigurationReference(storeConfigurationReference)
						.build();
			} else {
				result = AdmincowModel.builder()
						.objectModels(List.empty())
						.build();
			}
		}
		return result;
	}

	public Component render(Parameters parameters) {
		AdmincowModel value = findOrCreateModel(parameters.valueReference(), parameters.exploreString());
		//
		return render(State.builder()
				.currentValue(value)
				.initialValue(value.getUid() != null
						? value
						: AdmincowModel.builder()
						.objectModels(List.empty())
						.build())
				.parameters(parameters)
				.build());
	}

	public Component render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		String exploreString;

		@NonNull
		Consumer<UidReference<AdmincowModel>> onValueReferenceChange;

		@Nullable
		UidReference<AdmincowModel> valueReference;

		public static class Builder {
			// Lomboked
		}
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class State {

		@NonNull
		AdmincowModel currentValue;

		@NonNull
		AdmincowModel initialValue;

		@NonNull
		Parameters parameters;
	}
}
