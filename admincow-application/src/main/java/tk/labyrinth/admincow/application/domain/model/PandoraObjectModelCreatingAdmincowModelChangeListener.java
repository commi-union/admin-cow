package tk.labyrinth.admincow.application.domain.model;

import io.vavr.collection.List;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.admincow.application.domain.store.AdmincowStoreUtils;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.index.PandoraIndexConstants;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.origin.PandoraOriginUtils;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.domain.mongodb.PandoraMongoDbUtils;
import tk.labyrinth.pandora.stores.domain.rootobject.SignatureObjectModelAttributeFeature;
import tk.labyrinth.pandora.stores.listener.TypedObjectChangeEvent;
import tk.labyrinth.pandora.stores.listener.TypedObjectChangeListener;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;
import tk.labyrinth.pandora.stores.rootobject.UidReference;

import java.util.Objects;

@Bean
@Slf4j
public class PandoraObjectModelCreatingAdmincowModelChangeListener extends
		TypedObjectChangeListener<AdmincowModel, UidReference<AdmincowModel>> {

	@SmartAutowired
	private TypedObjectManipulator<ObjectModel> objectModelManipulator;

	@Override
	// FIXME: Change to protected after proper reindexing mechanism (see usages).
	public void onObjectChange(TypedObjectChangeEvent<AdmincowModel, UidReference<AdmincowModel>> event) {
		Reference<AdmincowModel> primaryReference = event.getPrimaryReference();
		//
		if (event.hasPreviousObject()) {
			objectModelManipulator.delete(PlainQuery.builder()
					.predicate(PandoraOriginUtils.createPredicate(primaryReference))
					.build());
		}
		if (event.hasNextObject()) {
			AdmincowModel admincowModel = event.getNextObjectOrFail();
			//
			GenericObjectAttribute originAttribute = PandoraOriginUtils.createAttribute(primaryReference);
			//
			{
				try {
					List<ObjectModel> objectModels = admincowModel.getObjectModels()
							.zipWithIndex()
							.map(tuple -> {
								ObjectModel objectModel = tuple._1();
								//
								return tuple._2() == 0
										? objectModel.toBuilder()
										.attributes(objectModel.getAttributes().map(attribute -> Objects.equals(attribute.getName(), "_id")
												? attribute.withFeatures(attribute.getFeaturesOrEmpty().append(
												SignatureObjectModelAttributeFeature.instance()))
												: attribute))
										.tags(objectModel.getTagsOrEmpty().append(PandoraMongoDbUtils.createCollectionNameTag(
												objectModel.getCode().substring(AdmincowStoreUtils.composeObjectModelCodePrefix(
														admincowModel.getStoreConfigurationReference()).length()))))
										.build()
										: objectModel;
							});
					//
					objectModels.forEach(objectModel -> objectModelManipulator.createWithAdjustment(
							objectModel,
							genericObject -> genericObject.withAddedAttributes(
									originAttribute,
									PandoraIndexConstants.ATTRIBUTE)));
				} catch (RuntimeException ex) {
					// FIXME: There should be a generic mechanism to catch failed listeners and report this,
					//  no need to catch anything here.
					logger.error("", ex);
				}
			}
		}
	}
}
