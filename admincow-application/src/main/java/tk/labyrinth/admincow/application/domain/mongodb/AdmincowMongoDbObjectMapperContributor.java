package tk.labyrinth.admincow.application.domain.mongodb;

import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.domain.mongodb.mapper.MongoDbObjectMapper;
import tk.labyrinth.pandora.stores.domain.mongodb.mapper.MongoDbObjectMapperContributor;
import tk.labyrinth.pandora.stores.domain.mongodb.mapper.NoOpMongoDbObjectMapper;

@Bean
public class AdmincowMongoDbObjectMapperContributor implements MongoDbObjectMapperContributor {

	@Override
	public MongoDbObjectMapper contributeMongoDbObjectMapper(CodeObjectModelReference target) {
		return NoOpMongoDbObjectMapper.INSTANCE;
	}

	@Nullable
	@Override
	public Integer getSupportDistance(CodeObjectModelReference target) {
		return target.getCode().startsWith("admincow:") ? 1000 : null;
	}
}
