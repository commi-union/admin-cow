package tk.labyrinth.admincow.application.domain.mongodb;

import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import io.vavr.collection.List;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.admincow.application.domain.common.NameChain;
import tk.labyrinth.admincow.application.domain.model.AdmincowModel;
import tk.labyrinth.admincow.application.domain.model.AdmincowModelUtils;
import tk.labyrinth.admincow.application.domain.store.CollectionDescription;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModelUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttribute;
import tk.labyrinth.pandora.stores.domain.mongodb.MongoDbDatatypeConstants;
import tk.labyrinth.pandora.stores.domain.rootobject.RootObjectModelFeature;
import tk.labyrinth.pandora.stores.init.customdatatype.PandoraDatatypeConstants;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

// TODO: Look at aggregations: https://www.objectrocket.com/blog/how-to/get-keys-mongodb-collection/
public class MongoDbUtils {

	public static Pair<ObjectModelAttribute, List<ObjectModel>> createObjectModelAttribute(
			String objectModelCodePrefix,
			NameChain nameChain,
			List<Object> values) {
		Pair<Datatype, List<ObjectModel>> pair = determineDatatype(objectModelCodePrefix, nameChain, values);
		//
		return Pair.of(
				ObjectModelAttribute.builder()
						.datatype(pair.getLeft())
						.name(nameChain.getLast())
						.build(),
				pair.getRight());
	}

	// TODO: Merge into Satool's JsonAnalyzer.
	public static List<ObjectModel> createObjectModelsFromDocuments(
			String objectModelCodePrefix,
			NameChain nameChain,
			List<Document> documents,
			boolean root) {
		List<ObjectModel> result;
		{
			List<Pair<ObjectModelAttribute, List<ObjectModel>>> pairs = documents
					.flatMap(Document::entrySet)
					.groupBy(Map.Entry::getKey)
					.mapValues(value -> value.map(Map.Entry::getValue))
					.map(tuple -> createObjectModelAttribute(objectModelCodePrefix, nameChain.append(tuple._1()), tuple._2()))
					.toList();
			//
			result = List
					.of(ObjectModel.builder()
							.attributes(pairs.map(Pair::getLeft))
							.code(AdmincowModelUtils.composeObjectModelCode(objectModelCodePrefix, nameChain))
							.features(root
									? List.of(RootObjectModelFeature.of("_id"))
									: List.empty())
							.name(StringUtils.capitalize(nameChain.getLast()))
							.build())
					.appendAll(pairs.flatMap(Pair::getRight));
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static Pair<Datatype, List<ObjectModel>> determineDatatype(
			String objectModelCodePrefix,
			NameChain nameChain,
			List<?> values) {
		Pair<Datatype, List<ObjectModel>> result;
		{
			List<@Nullable ? extends Class<?>> javaClasses = values.map(value -> value != null ? value.getClass() : null)
					.distinct()
					.filter(Objects::nonNull); // TODO: Do smth with nulls. Probably add a mark that null values are registered and store them as well.
			//
			if (javaClasses.size() != 1) {
				throw new NotImplementedException();
			}
			//
			Class<?> javaClass = javaClasses.single();
			//
			if (javaClass == ArrayList.class) {
				Pair<Datatype, List<ObjectModel>> pair = determineDatatype(
						objectModelCodePrefix,
						nameChain,
						values.map(ArrayList.class::cast).flatMap(List::ofAll));
				//
				result = Pair.of(
						Datatype.builder()
								.baseReference(AliasDatatypeBaseReference.of(PandoraDatatypeConstants.LIST_DATATYPE_NAME))
								.parameters(List.of(pair.getLeft()))
								.build(),
						pair.getRight());
			} else if (javaClass == Date.class) {
				result = Pair.of(
						MongoDbDatatypeConstants.DATE_DATATYPE,
						List.empty());
			} else if (javaClass == Document.class) {
				List<ObjectModel> objectModels = createObjectModelsFromDocuments(
						objectModelCodePrefix,
						nameChain,
						values.map(Document.class::cast),
						false);
				//
				result = Pair.of(
						Datatype.builder()
								.baseReference(ObjectModelUtils.createDatatypeBaseReference(
										AdmincowModelUtils.composeObjectModelCode(objectModelCodePrefix, nameChain)))
								.build(),
						objectModels);
			} else if (javaClass == Double.class) {
				result = Pair.of(
						Datatype.builder()
								.baseReference(JavaBaseTypeUtils.createDatatypeBaseReference(Double.class))
								.build(),
						List.empty());
			} else if (javaClass == Integer.class) {
				result = Pair.of(
						Datatype.builder()
								.baseReference(JavaBaseTypeUtils.createDatatypeBaseReference(Integer.class))
								.build(),
						List.empty());
			} else if (javaClass == ObjectId.class) {
				result = Pair.of(
						MongoDbDatatypeConstants.OBJECT_ID_DATATYPE,
						List.empty());
			} else if (javaClass == String.class) {
				result = Pair.of(
						Datatype.builder()
								.baseReference(AliasDatatypeBaseReference.of(PandoraDatatypeConstants.SIMPLE_DATATYPE_NAME))
								.build(),
						List.empty());
			} else {
				throw new NotImplementedException();
			}
		}
		//
		return result;
	}

	public static CollectionDescription exploreMongoCollection(
			String connectionString,
			String collectionName,
			String objectModelCodePrefix) {
		ConnectionString connectionStringObject = new ConnectionString(connectionString);
		//
		try (MongoClient mongoClient = MongoClients.create(connectionString)) {
			MongoDatabase mongoDatabase = mongoClient.getDatabase(connectionStringObject.getDatabase());
			//
			MongoCollection<Document> mongoCollection = mongoDatabase.getCollection(collectionName, Document.class);
			//
			List<Document> documents = List.ofAll(mongoCollection.find().limit(10));
			//
			List<ObjectModel> objectModels = createObjectModelsFromDocuments(
					objectModelCodePrefix,
					NameChain.of(collectionName),
					documents,
					true);
			//
			return CollectionDescription.builder()
					.model(AdmincowModel.builder()
							.collectionName(collectionName)
							.objectModels(objectModels)
							.build())
					.name(collectionName)
					.build();
		}
	}

	public static List<CollectionDescription> exploreMongoDatabase(String connectionString) {
		ConnectionString connectionStringObject = new ConnectionString(connectionString);
		//
		try (MongoClient mongoClient = MongoClients.create(connectionString)) {
			MongoDatabase mongoDatabase = mongoClient.getDatabase(connectionStringObject.getDatabase());
			//
			List<Document> collections = List.ofAll(mongoDatabase.listCollections());
			//
			return collections.map(collection -> CollectionDescription.builder()
					.name(collection.getString("name"))
					.build());
		}
	}
}
