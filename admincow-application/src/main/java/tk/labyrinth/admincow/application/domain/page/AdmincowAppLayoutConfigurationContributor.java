package tk.labyrinth.admincow.application.domain.page;

import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.VaadinServiceInitListener;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.admincow.application.domain.model.ConfigureModelPage;
import tk.labyrinth.admincow.application.domain.sidebar.AdmincowSidebarConfigurationPage;
import tk.labyrinth.admincow.application.domain.store.ConfigureStorePage;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.init.hardcoded.PandoraHardcodedConstants;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.ui.domain.applayout.AppLayoutConfigurationRegistry;
import tk.labyrinth.pandora.ui.domain.applayout.PandoraAppLayoutConfiguration;
import tk.labyrinth.pandora.ui.domain.route.PandoraRouteVaadinTool;
import tk.labyrinth.pandora.ui.domain.sidebar.PandoraSidebarItem;
import tk.labyrinth.pandora.ui.domain.sidebar.SidebarCategoryConfiguration;
import tk.labyrinth.pandora.ui.domain.sidebar.SidebarConfiguration;
import tk.labyrinth.pandora.ui.domain.sidebar.SlugSidebarCategoryConfigurationReference;

import java.util.Objects;

@Bean
@RequiredArgsConstructor
public class AdmincowAppLayoutConfigurationContributor implements VaadinServiceInitListener {

	private final AppLayoutConfigurationRegistry appLayoutConfigurationRegistry;

	private final PandoraRouteVaadinTool pandoraRouteVaadinTool;

	@SmartAutowired
	private TypedObjectManipulator<SidebarCategoryConfiguration> sidebarCategoryConfigurationManipulator;

	@SmartAutowired
	private TypedObjectSearcher<SidebarCategoryConfiguration> sidebarCategoryConfigurationSearcher;

	private PandoraSidebarItem leaf(String text, Class<?> target) {
		return PandoraSidebarItem.builder()
				.target(pandoraRouteVaadinTool.getPath(target))
				.text(text)
				.build();
	}

	// FIXME: We use Vaadin-bound solution because it is initialized later that PandoraInitializer.
	//  Need to find a way to make Pandora init at the end, or probably get rid of strict dependency.
	@Override
	public void serviceInit(ServiceInitEvent event) {
		{
			// FIXME: It seems that we can not manage the order in which listeners are invoked, so we make sure this one is initialized.
			pandoraRouteVaadinTool.serviceInit(event);
		}
		//
		SidebarCategoryConfiguration admincowMainConfiguration = sidebarCategoryConfigurationSearcher.findSingle(
				SlugSidebarCategoryConfigurationReference.of("admincow-main"));
		//
		sidebarCategoryConfigurationManipulator.createWithAdjustment(
				SidebarCategoryConfiguration.builder()
						.items(List.of(
								PandoraSidebarItem.builder()
										.text("Home")
//										.target(pandoraRouteVaadinTool.getPath(IndexDashboardPage.class)) // TODO: Solve empty path issue.
										.build(),
								leaf("Stores", ConfigureStorePage.class),
								leaf("Models", ConfigureModelPage.class),
								leaf("Sidebar", AdmincowSidebarConfigurationPage.class)))
						.slug("admincow-configuration")
						.text("AdminCow Configuration")
						.build(),
				genericObject -> genericObject.withAddedAttribute(PandoraHardcodedConstants.ATTRIBUTE));
		//
		appLayoutConfigurationRegistry.setAppLayoutConfiguration(PandoraAppLayoutConfiguration.builder()
				.priority(1)
				.sidebar(SidebarConfiguration.builder()
						.categoryReferences(List
								.of(
										admincowMainConfiguration != null
												? SlugSidebarCategoryConfigurationReference.from(admincowMainConfiguration)
												: null,
										SlugSidebarCategoryConfigurationReference.of("admincow-configuration"),
										SlugSidebarCategoryConfigurationReference.of("pandora-default"))
								.filter(Objects::nonNull))
						.mode(SidebarConfiguration.SidebarMode.SLIDE)
						.build())
				.useHeader(false)
				.build());
	}
}
