package tk.labyrinth.admincow.application.domain.reindex;

import jakarta.annotation.Priority;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.admincow.application.domain.model.AdmincowModel;
import tk.labyrinth.admincow.application.domain.model.PandoraObjectModelCreatingAdmincowModelChangeListener;
import tk.labyrinth.admincow.application.domain.store.AdmincowStoreConfiguration;
import tk.labyrinth.admincow.application.domain.store.PandoraStoreConfigurationCreatingAdmincowStoreConfigurationChangeListener;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.init.PandoraInitializer;
import tk.labyrinth.pandora.stores.listener.ObjectChangeEvent;
import tk.labyrinth.pandora.stores.listener.TypedObjectChangeEvent;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.stores.rootobject.UidReference;

// FIXME: Should be reworked into generic indexing mechanism.
@LazyComponent
@Priority(5000)
@RequiredArgsConstructor
public class AdmincowReindexingInitializer implements PandoraInitializer {

	private final PandoraObjectModelCreatingAdmincowModelChangeListener modelChangeListener;

	private final PandoraStoreConfigurationCreatingAdmincowStoreConfigurationChangeListener storeConfigurationChangeListener;

	@SmartAutowired
	private TypedObjectSearcher<AdmincowModel> modelSearcher;

	@SmartAutowired
	private TypedObjectSearcher<AdmincowStoreConfiguration> storeConfigurationSearcher;

	@Override
	public void run() {
		storeConfigurationSearcher.searchAll().forEach(storeConfiguration -> storeConfigurationChangeListener
				.onObjectChange(ObjectChangeEvent.<AdmincowStoreConfiguration>builder()
						.nextObject(storeConfiguration)
						.previousObject(null)
						.primaryReference(UidReference.of(AdmincowStoreConfiguration.MODEL_CODE, storeConfiguration.getUid()))
						.build()));
		modelSearcher.searchAll().forEach(model -> modelChangeListener.onObjectChange(
				TypedObjectChangeEvent.<AdmincowModel, UidReference<AdmincowModel>>builder()
						.nextObject(model)
						.previousObject(null)
						.primaryReference(UidReference.of(AdmincowModel.MODEL_CODE, model.getUid()))
						.build()));
	}
}
