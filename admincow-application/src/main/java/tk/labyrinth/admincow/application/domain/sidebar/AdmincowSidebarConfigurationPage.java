package tk.labyrinth.admincow.application.domain.sidebar;

import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dnd.DragSource;
import com.vaadin.flow.component.dnd.EffectAllowed;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.grid.dnd.GridDropEvent;
import com.vaadin.flow.component.grid.dnd.GridDropLocation;
import com.vaadin.flow.component.grid.dnd.GridDropMode;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.router.Route;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.admincow.application.domain.model.AdmincowModel;
import tk.labyrinth.admincow.application.domain.model.AdmincowModelUtils;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.functionalcomponents.advanced.TreeGridWrapper;
import tk.labyrinth.pandora.functionalcomponents.html.AnchorRenderer;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponent2;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.exception.UnreachableStateException;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.stores.rootobject.UidReference;
import tk.labyrinth.pandora.ui.domain.applayout.ConfigurableAppLayout;
import tk.labyrinth.pandora.ui.domain.form.FormsTool;
import tk.labyrinth.pandora.ui.domain.sidebar.PandoraSidebarItem;
import tk.labyrinth.pandora.ui.domain.sidebar.SidebarCategoryConfiguration;
import tk.labyrinth.pandora.ui.domain.sidebar.SlugSidebarCategoryConfigurationReference;
import tk.labyrinth.pandora.views.style.PandoraStyles;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@Route(value = "sidebar-configuration", layout = ConfigurableAppLayout.class)
public class AdmincowSidebarConfigurationPage extends CssVerticalLayout {

	private final FormsTool formsTool;

	@SmartAutowired
	private TypedObjectSearcher<AdmincowModel> admincowModelSearcher;

	@Nullable
	private String dragData = null;

	@SmartAutowired
	private TypedObjectManipulator<SidebarCategoryConfiguration> sidebarCategoryConfigurationManipulator;

	@SmartAutowired
	private TypedObjectSearcher<SidebarCategoryConfiguration> sidebarCategoryConfigurationSearcher;

	private PandoraSidebarItem computeDragItem(@Nullable String dragData) {
		PandoraSidebarItem result;
		{
			if (dragData != null) {
				if (Objects.equals(dragData, "new_item")) {
					result = PandoraSidebarItem.builder().build();
				} else if (dragData.startsWith("model:")) {
					AdmincowModel admincowModel = admincowModelSearcher.getSingle(UidReference.of(
							AdmincowModel.MODEL_CODE,
							UUID.fromString(dragData.substring("model:".length()))));
					//
					String rootObjectModelCode = AdmincowModelUtils.computeRootObjectModelCode(admincowModel);
					//
					result = PandoraSidebarItem.builder()
							.target("pandora/objects/%s".formatted(rootObjectModelCode))
							.text(admincowModel.getObjectModels().get(0).getName())
							.build();
				} else {
					throw new NotImplementedException();
				}
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	private List<PandoraSidebarItem> computeNextState(
			List<PandoraSidebarItem> currentState,
			GridDropEvent<TreeGridWrapper.ItemWrapper<PandoraSidebarItem>> event,
			String dragData) {
		List<PandoraSidebarItem> result;
		{
			PandoraSidebarItem dragItem = computeDragItem(dragData);
			//
			if (event.getDropLocation() != GridDropLocation.EMPTY) {
				result = update(
						currentState,
						event.getDropTargetItem().orElseThrow().getItem(),
						dragItem,
						event.getDropLocation());
			} else {
				result = List.of(dragItem);
			}
		}
		return result;
	}

	@PostConstruct
	private void postConstruct() {
		val stateObservable = Observable.withInitialValue(Optional
				.ofNullable(sidebarCategoryConfigurationSearcher.findSingle(
						SlugSidebarCategoryConfigurationReference.of("admincow-main")))
				.map(configuration -> Pair.of(configuration, configuration))
				.orElseGet(() -> {
					SidebarCategoryConfiguration newValue = SidebarCategoryConfiguration.builder()
							.items(List.empty())
							.slug("admincow-main")
							.text("AdminCow Main")
							.build();
					//
					return Pair.of(newValue, newValue);
				}));
		//
		{
			{
				CssGridLayout mainLayout = new CssGridLayout();
				{
					mainLayout.setGridTemplateAreas(
							"header header",
							"grid options");
					mainLayout.setGridTemplateColumns("1fr auto");
					mainLayout.setGridTemplateRows("auto 1fr");
					//
					mainLayout.addClassName(PandoraStyles.LAYOUT);
					mainLayout.setHeightFull();
				}
				{
					{
						CssHorizontalLayout headerLayout = new CssHorizontalLayout();
						{
						}
						{
							headerLayout.add(FunctionalComponent2.of(
									stateObservable,
									(state, sink) -> ButtonRenderer.render(builder -> builder
											.enabled(!Objects.equals(state.getLeft(), state.getRight()))
											.onClick(event -> {
												sidebarCategoryConfigurationManipulator.createOrUpdate(state.getRight());
												//
												sink.accept(Pair.of(state.getRight(), state.getRight()));
											})
											.text("Save")
											.build())));
						}
						mainLayout.add(headerLayout, "header");
					}
					{
						TreeGridWrapper<PandoraSidebarItem> configurationGrid = new TreeGridWrapper<>(
								PandoraSidebarItem::getChildrenOrEmpty,
								item -> item.withChildren(null),
								System::identityHashCode);
						{
							configurationGrid.getContent().setDropMode(GridDropMode.ON_TOP_OR_BETWEEN);
							configurationGrid.getContent().setHeightFull();
							configurationGrid.getContent().setRowsDraggable(true);
							configurationGrid.setSelectionMode(Grid.SelectionMode.NONE);
							configurationGrid.addThemeVariants(
									GridVariant.LUMO_COMPACT,
									GridVariant.LUMO_NO_BORDER,
									GridVariant.LUMO_NO_ROW_BORDERS);
						}
						{
							configurationGrid
									.addHierarchyColumn(item -> item.getText() != null ? item.getText() : "<empty>")
									.setHeader("Text")
									.setResizable(true);
							configurationGrid
									.addComponentColumn(item -> item.getTarget() != null
											? AnchorRenderer.render(builder -> builder
											.href(item.getTarget())
											.text(item.getTarget())
											.build())
											: new Div())
									.setHeader("Target")
									.setResizable(true);
							configurationGrid
									.addComponentColumn(item -> ButtonRenderer.render(builder -> builder
											.icon(VaadinIcon.COG.create())
											.onClick(event -> formsTool
													.showDialog(item)
													.subscribeAlwaysAccepted(result -> stateObservable.update(currentState -> Pair.of(
															currentState.getLeft(),
															currentState.getRight().withItems(
																	computeNextState(currentState.getRight().getItems(), item, result))))))
											.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY_INLINE)
											.build()))
									.setFlexGrow(0);
						}
						{
							configurationGrid.getContent().addDropListener(event -> {
								stateObservable.update(currentState -> Pair.of(
										currentState.getLeft(),
										currentState.getRight().withItems(
												computeNextState(currentState.getRight().getItems(), event, dragData))));
								//
								dragData = null;
							});
						}
						{
							stateObservable.subscribe(nextState -> {
								configurationGrid.setItems(nextState.getRight().getItems());
								//
								configurationGrid.expandAllItems();
							});
						}
						mainLayout.add(configurationGrid, "grid");
					}
					{
						CssVerticalLayout optionsLayout = new CssVerticalLayout();
						{
							optionsLayout.addClassName(PandoraStyles.LAYOUT);
						}
						{
							{
								Span newItemSpan = new Span("New Item");
								{
									newItemSpan.addClassNames(PandoraStyles.CARD, PandoraStyles.LAYOUT_SMALL);
								}
								{
									DragSource<Span> dragSource = DragSource.create(newItemSpan);
									dragSource.addDragStartListener(event -> dragData = "new_item");
									dragSource.setEffectAllowed(EffectAllowed.COPY);
								}
								optionsLayout.add(newItemSpan);
							}
							{
								List<AdmincowModel> admincowModels = admincowModelSearcher.searchAll();
								//
								admincowModels.forEach(admincowModel -> {
									Span modelSpan = new Span(admincowModel.getObjectModels().get(0).getName());
									{
										modelSpan.addClassNames(PandoraStyles.CARD, PandoraStyles.LAYOUT_SMALL);
									}
									{
										DragSource<Span> dragSource = DragSource.create(modelSpan);
										dragSource.addDragStartListener(event -> dragData = "model:%s".formatted(admincowModel.getUid()));
										dragSource.setEffectAllowed(EffectAllowed.COPY);
									}
									optionsLayout.add(modelSpan);
								});
							}
						}
						mainLayout.add(optionsLayout, "options");
					}
				}
				add(mainLayout);
			}
		}
	}

	public static List<PandoraSidebarItem> computeNextState(
			List<PandoraSidebarItem> items,
			PandoraSidebarItem currentItem,
			PandoraSidebarItem nextItem) {
		int index = items.indexWhere(item -> System.identityHashCode(item) == System.identityHashCode(currentItem));
		//
		return index != -1
				? items.update(index, nextItem)
				: items.map(item -> item.withChildren(computeNextState(item.getChildrenOrEmpty(), currentItem, nextItem)));
	}

	public static List<PandoraSidebarItem> update(
			List<PandoraSidebarItem> items,
			PandoraSidebarItem dropItem,
			PandoraSidebarItem dragItem,
			GridDropLocation dropLocation) {
		int index = items.indexWhere(item -> System.identityHashCode(item) == System.identityHashCode(dropItem));
		//
		return index != -1
				? switch (dropLocation) {
			case ABOVE -> items.insert(index, dragItem);
			case BELOW -> !dropItem.getChildrenOrEmpty().isEmpty()
					? items.update(index, dropItem.withChildren(dropItem.getChildrenOrEmpty().prepend(dragItem)))
					: items.insert(index + 1, dragItem);
			case ON_TOP -> items.update(index, item -> item.withChildren(item.getChildrenOrEmpty().prepend(dragItem)));
			case EMPTY -> throw new UnreachableStateException();
		}
				: items.map(item -> item.withChildren(update(item.getChildrenOrEmpty(), dropItem, dragItem, dropLocation)));
	}
}
