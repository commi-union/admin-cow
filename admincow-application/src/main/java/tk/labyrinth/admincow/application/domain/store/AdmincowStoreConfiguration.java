package tk.labyrinth.admincow.application.domain.store;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.render.RenderAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.root.RootModel;
import tk.labyrinth.pandora.stores.model.StoreKind;
import tk.labyrinth.pandora.stores.rootobject.HasUid;

import java.net.URI;
import java.util.UUID;

@Builder(builderClassName = "Builder", toBuilder = true)
@RenderAttribute("name")
@RootModel(value = AdmincowStoreConfiguration.MODEL_CODE, primaryAttribute = HasUid.UID_ATTRIBUTE_NAME)
@Value
@With
public class AdmincowStoreConfiguration implements HasUid {

	public static final String MODEL_CODE = "admincowstoreconfiguration";

	public static final String NAME_ATTRIBUTE_NAME = "name";

	Boolean enabled;

	@Nullable
	StoreKind explicitKind;

	String name;

	UUID uid;

	String url;

	@Nullable
	public StoreKind computeKind() {
		StoreKind result;
		{
			if (explicitKind != null) {
				result = explicitKind;
			} else {
				if (url != null) {
					result = StoreKind.resolveStoreKind(URI.create(url));
				} else {
					result = null;
				}
			}
		}
		return result;
	}
}