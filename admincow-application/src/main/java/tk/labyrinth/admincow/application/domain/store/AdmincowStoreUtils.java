package tk.labyrinth.admincow.application.domain.store;

import io.vavr.collection.List;
import tk.labyrinth.admincow.application.domain.mongodb.MongoDbUtils;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;

public class AdmincowStoreUtils {

	public static String composeObjectModelCodePrefix(AdmincowStoreConfiguration storeConfiguration) {
		return "admincow:%s:".formatted(storeConfiguration.getName());
	}

	public static String composeObjectModelCodePrefix(
			NameAdmincowStoreConfigurationReference storeConfigurationReference) {
		return "admincow:%s:".formatted(storeConfigurationReference.getName());
	}

	public static List<CollectionDescription> exploreStore(AdmincowStoreConfiguration storeConfiguration) {
		return switch (storeConfiguration.computeKind()) {
			case GIT -> throw new NotImplementedException();
			case IN_MEMORY -> throw new NotImplementedException();
			case MONGO_DB -> MongoDbUtils.exploreMongoDatabase(storeConfiguration.getUrl());
		};
	}
}
