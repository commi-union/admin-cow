package tk.labyrinth.admincow.application.domain.store;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.admincow.application.domain.model.AdmincowModel;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class CollectionDescription {

	@Nullable
	Integer count;

	AdmincowModel model;

	String name;
}
