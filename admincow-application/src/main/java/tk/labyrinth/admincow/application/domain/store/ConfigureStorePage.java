package tk.labyrinth.admincow.application.domain.store;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.AnchorTarget;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.router.Route;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.reflect.TypeUtils;
import tk.labyrinth.admincow.application.domain.model.ConfigureModelPage;
import tk.labyrinth.admincow.application.domain.model.ConfigureModelTool;
import tk.labyrinth.admincow.application.domain.mongodb.MongoDbUtils;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalPage;
import tk.labyrinth.pandora.functionalcomponents.vaadin.RouterLinkRenderer;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.functionalvaadin.component.CheckboxRenderer;
import tk.labyrinth.pandora.misc4j.java.util.function.FunctionUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.stores.model.StoreKind;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.stores.rootobject.UidReference;
import tk.labyrinth.pandora.ui.component.dialog.Dialogs;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxRegistry;
import tk.labyrinth.pandora.ui.domain.applayout.ConfigurableAppLayout;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.pandora.ui.tool.VaadinSessionCache;

import java.util.Objects;
import java.util.UUID;
import java.util.function.Consumer;

@RequiredArgsConstructor
@Route(value = "configure-store", layout = ConfigurableAppLayout.class)
public class ConfigureStorePage extends FunctionalPage<ConfigureStorePage.State> {

	private final ConfigureModelTool configureModelTool;

	private final VaadinSessionCache vaadinSessionCache;

	private final ValueBoxRegistry valueBoxRegistry;

	@SmartAutowired
	private TypedObjectManipulator<AdmincowStoreConfiguration> storeConfigurationManipulator;

	@SmartAutowired
	private TypedObjectSearcher<AdmincowStoreConfiguration> storeConfigurationSearcher;

	@Override
	protected State getInitialProperties() {
		AdmincowStoreConfiguration newObject = AdmincowStoreConfiguration.builder()
				.enabled(false)
				.build();
		//
		return State.builder()
				.autoExplore(false)
				.currentValue(newObject)
				.initialValue(newObject)
				.build();
	}

	@Override
	protected Component render(State state, Consumer<State> sink) {
		AdmincowStoreConfiguration currentValue = state.currentValue();
		//
		CssGridLayout layout = new CssGridLayout();
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
			//
			layout.setAlignItems(AlignItems.BASELINE);
			layout.setGridTemplateAreas(
					"selector name enabled save . .",
					"url url url url url url",
					"explicitKind computedKind explore . . .",
					"collections collections collections collections collections collections");
			layout.setGridTemplateColumns("repeat(1fr,6)");
			layout.setGridTemplateRows("auto auto auto 1fr");
		}
		Observable<List<CollectionDescription>> exploredCollectionDescriptionsObservable = Observable.withInitialValue(
				List.empty());
		{
			{
				MutableValueBox<UidReference<AdmincowStoreConfiguration>> storeConfigurationReferenceBox =
						valueBoxRegistry.getValueBoxInferred(
								TypeUtils.parameterize(UidReference.class, AdmincowStoreConfiguration.class));
				//
				storeConfigurationReferenceBox.render(builder -> builder
						.currentValue(currentValue.getUid() != null
								? UidReference.of(AdmincowStoreConfiguration.MODEL_CODE, currentValue.getUid())
								: null)
						.initialValue(null)
						.label("Store Configuration")
						.onValueChange(nextValue -> {
							AdmincowStoreConfiguration selectedValue = nextValue != null
									? storeConfigurationSearcher.getSingle(nextValue)
									: AdmincowStoreConfiguration.builder().build();
							//
							sink.accept(state.toBuilder()
									.currentValue(selectedValue)
									.initialValue(selectedValue)
									.build());
						})
						.build());
				//
				layout.add(storeConfigurationReferenceBox.asVaadinComponent(), "selector");
			}
			{
				MutableValueBox<String> nameBox = valueBoxRegistry.getValueBox(String.class);
				//
				nameBox.render(MutableValueBox.Properties.<String>builder()
						.currentValue(currentValue.getName())
						.initialValue(state.initialValue().getName())
						.label("Name")
						.onValueChange(nextValue -> sink.accept(state.withCurrentValue(currentValue.withName(
								nextValue))))
						.build());
				//
				layout.add(nameBox.asVaadinComponent(), "name");
			}
			{
				layout.add(
						CheckboxRenderer.render(builder -> builder
								.enabled(state.currentValue().getName() != null)
								.label("Enabled")
								.onValueChange(nextValue -> sink.accept(state.withCurrentValue(currentValue.withEnabled(nextValue))))
								.value(BooleanUtils.isTrue(state.currentValue().getEnabled()))
								.build()),
						"enabled");
			}
			{
				layout.add(
						ButtonRenderer.render(builder -> builder
								.disableOnClick(true)
								.enabled(!Objects.equals(currentValue, state.initialValue()))
								.onClick(event -> {
									AdmincowStoreConfiguration storeToSave = currentValue.getUid() != null
											? currentValue
											: currentValue.withUid(UUID.randomUUID());
									//
									storeConfigurationManipulator.createOrUpdate(storeToSave);
									//
									sink.accept(state.toBuilder()
											.currentValue(storeToSave)
											.initialValue(storeToSave)
											.build());
								})
								.text("Save")
								.themeVariants(ButtonVariant.LUMO_PRIMARY)
								.build()),
						"save");
			}
			{
				MutableValueBox<String> urlBox = valueBoxRegistry.getValueBox(String.class);
				//
				urlBox.render(builder -> builder
						.currentValue(currentValue.getUrl())
						.initialValue(state.initialValue().getUrl())
						.label("URL")
						.onValueChange(nextValue -> sink.accept(state.withCurrentValue(currentValue.withUrl(
								nextValue))))
						.build());
				//
				layout.add(urlBox.asVaadinComponent(), "url");
			}
			{
				MutableValueBox<StoreKind> explicitKindBox = valueBoxRegistry.getValueBox(StoreKind.class);
				//
				explicitKindBox.render(builder -> builder
						.currentValue(currentValue.getExplicitKind())
						.initialValue(state.initialValue().getExplicitKind())
						.label("Explicit Kind")
						.onValueChange(nextValue -> sink.accept(state.withCurrentValue(currentValue
								.withExplicitKind(nextValue))))
						.build());
				//
				layout.add(explicitKindBox.asVaadinComponent(), "explicitKind");
			}
			{
				MutableValueBox<StoreKind> computedKindBox = valueBoxRegistry.getValueBox(StoreKind.class);
				//
				computedKindBox.render(builder -> builder
						.currentValue(currentValue.computeKind())
						.initialValue(state.initialValue().computeKind())
						.label("Computed Kind")
						.nonEditableReasons(List.of("Read-only"))
						.onValueChange(FunctionUtils::throwUnreachableStateException)
						.build());
				//
				layout.add(computedKindBox.asVaadinComponent(), "computedKind");
			}
			{
				layout.add(
						ButtonRenderer.render(builder -> builder
								.enabled(canExplore(currentValue))
								.onClick(event -> performExplore(
										currentValue,
										exploredCollectionDescriptionsObservable::set))
								.text("Explore")
								.themeVariants(ButtonVariant.LUMO_PRIMARY)
								.build()),
						"explore");
			}
		}
		{
			Grid<CollectionDescription> collectionDescriptionsGrid = new Grid<>();
			{
				collectionDescriptionsGrid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
				//
				collectionDescriptionsGrid.setAllRowsVisible(true);
			}
			{
				collectionDescriptionsGrid
						.addColumn(CollectionDescription::getName)
						.setHeader("Name")
						.setResizable(true);
				collectionDescriptionsGrid
						.addComponentColumn(item -> ButtonRenderer.render(builder -> builder
								.text("Analyze Model")
								.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY)
								.onClick(event -> {
									CollectionDescription collectionDescription = MongoDbUtils.exploreMongoCollection(
											currentValue.getUrl(),
											item.getName(),
											AdmincowStoreUtils.composeObjectModelCodePrefix(currentValue));
									//
									Component component = configureModelTool.renderObjectModelsGrid(
											collectionDescription.getModel().getObjectModels());
									//
									component.getStyle().setWidth("80vw");
									//
									Dialogs.show(component);
								})
								.build()))
						.setResizable(true);
				collectionDescriptionsGrid
						.addComponentColumn(item -> RouterLinkRenderer.render(builder -> builder
								.queryParameters(QueryParameters.of(
										"explore",
										"%s:%s".formatted(currentValue.getName(), item.getName())))
								.route(ConfigureModelPage.class)
								.target(AnchorTarget.BLANK)
								.text("Configure Model")
								.build()))
						.setResizable(true);
			}
			{
				exploredCollectionDescriptionsObservable.subscribe(nextExploredCollections ->
						collectionDescriptionsGrid.setItems(nextExploredCollections.asJava()));
			}
			{
				layout.add(collectionDescriptionsGrid, "collections");
			}
		}
		{
			if (state.autoExplore() && canExplore(currentValue)) {
				performExplore(currentValue, exploredCollectionDescriptionsObservable::set);
			}
		}
		return layout;
	}

	private static boolean canExplore(AdmincowStoreConfiguration storeConfiguration) {
		return storeConfiguration.getUrl() != null && storeConfiguration.computeKind() != null;
	}

	private static void performExplore(
			AdmincowStoreConfiguration storeConfiguration,
			Consumer<List<CollectionDescription>> exploredCollectionDescriptionsSink) {
		List<CollectionDescription> collectionDescriptions = AdmincowStoreUtils.exploreStore(storeConfiguration);
		//
		exploredCollectionDescriptionsSink.accept(collectionDescriptions);
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class State {

		@NonNull
//		@UiStateAttribute(queryAttributeName = "autoexplore")
		Boolean autoExplore;

		@NonNull
		AdmincowStoreConfiguration currentValue;

		@NonNull
		AdmincowStoreConfiguration initialValue;
	}
}
