package tk.labyrinth.admincow.application.domain.store;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.vavr.collection.List;
import lombok.NonNull;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReferenceAttribute;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

import java.util.Objects;

@Value(staticConstructor = "of")
public class NameAdmincowStoreConfigurationReference implements Reference<AdmincowStoreConfiguration> {

	@NonNull
	String name;

	public GenericReference toGenericReference() {
		return GenericReference.of(
				AdmincowStoreConfiguration.MODEL_CODE,
				List.of(GenericReferenceAttribute.of(AdmincowStoreConfiguration.NAME_ATTRIBUTE_NAME, name)));
	}

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				AdmincowStoreConfiguration.MODEL_CODE,
				AdmincowStoreConfiguration.NAME_ATTRIBUTE_NAME,
				name);
	}

	public static NameAdmincowStoreConfigurationReference from(GenericReference reference) {
		if (!isNameAdmincowStoreConfigurationReference(reference)) {
			throw new IllegalArgumentException(reference.toString());
		}
		return of(reference.getAttributeValue(AdmincowStoreConfiguration.NAME_ATTRIBUTE_NAME));
	}

	public static NameAdmincowStoreConfigurationReference from(AdmincowStoreConfiguration value) {
		return of(value.getName());
	}

	// FIXME: Must be generic in ReferenceOMConfigurer
	@JsonCreator
	public static NameAdmincowStoreConfigurationReference from(String value) {
		return from(GenericReference.from(value));
	}

	public static boolean isNameAdmincowStoreConfigurationReference(GenericReference reference) {
		return Objects.equals(reference.getModelCode(), AdmincowStoreConfiguration.MODEL_CODE) &&
				reference.hasAttribute(AdmincowStoreConfiguration.NAME_ATTRIBUTE_NAME);
	}
}
