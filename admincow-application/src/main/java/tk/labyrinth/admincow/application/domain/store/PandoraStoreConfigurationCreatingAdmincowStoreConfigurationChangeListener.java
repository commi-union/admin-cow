package tk.labyrinth.admincow.application.domain.store;

import com.mongodb.ConnectionString;
import org.apache.commons.lang3.BooleanUtils;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.admincow.application.domain.model.AdmincowModelUtils;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.index.PandoraIndexConstants;
import tk.labyrinth.pandora.datatypes.domain.origin.PandoraOriginUtils;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.MongoDbStoreSettings;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;
import tk.labyrinth.pandora.stores.domain.storeroute.StoreRoute;
import tk.labyrinth.pandora.stores.listener.ObjectChangeEvent;
import tk.labyrinth.pandora.stores.listener.ObjectChangeListener;
import tk.labyrinth.pandora.stores.model.StoreDesignation;
import tk.labyrinth.pandora.stores.model.StoreKind;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;
//
// remove uid added to new objects.

@LazyComponent
public class PandoraStoreConfigurationCreatingAdmincowStoreConfigurationChangeListener extends
		ObjectChangeListener<AdmincowStoreConfiguration> {

	@SmartAutowired
	private TypedObjectManipulator<StoreConfiguration> storeConfigurationManipulator;

	@SmartAutowired
	private TypedObjectManipulator<StoreRoute> storeRouteManipulator;

	@Override
	// FIXME: Change to protected after proper reindexing mechanism (see usages).
	public void onObjectChange(ObjectChangeEvent<AdmincowStoreConfiguration> event) {
		if (event.hasPreviousObject()) {
			Predicate originPredicate = PandoraOriginUtils.createPredicate(event.getPrimaryReference());
			//
			storeConfigurationManipulator.delete(originPredicate);
			storeRouteManipulator.delete(originPredicate);
		}
		if (event.hasNextObject()) {
			AdmincowStoreConfiguration admincowStoreConfiguration = event.getNextObjectOrFail();
			//
			if (BooleanUtils.isTrue(admincowStoreConfiguration.getEnabled())) {
				StoreKind storeKind = admincowStoreConfiguration.computeKind();
				//
				if (storeKind != null) {
					Predicate objectModelReferencePredicate = AdmincowModelUtils.computeRootObjectModelReferencePredicate(
							admincowStoreConfiguration);
					//
					StoreConfiguration rawStoreConfiguration = StoreConfiguration.builder()
							.designation(StoreDesignation.COMMON)
							.kind(storeKind)
							.predicate(
									objectModelReferencePredicate
									// FIXME: We want to use this one, but first we need to implement proper store filtering by predicate
									//  that will ensure no additional stores match when core ones are expected.
//								Predicates.equalTo("admincowStoreName", admincowStoreConfiguration.getName())
							)
							.slug(admincowStoreConfiguration.getName())
							.build();
					//
					StoreConfiguration storeConfigurationToSave = switch (storeKind) {
						case MONGO_DB -> {
							MongoDbStoreSettings mongoDbSettings = resolveMongoDbSettings(admincowStoreConfiguration);
							//
							yield mongoDbSettings != null ? rawStoreConfiguration.withMongoDbSettings(mongoDbSettings) : null;
						}
						default -> throw new NotImplementedException();
					};
					//
					if (storeConfigurationToSave != null) {
						GenericObjectAttribute originAttribute = PandoraOriginUtils.createAttribute(event.getPrimaryReference());
						//
						storeConfigurationManipulator.createWithAdjustment(
								storeConfigurationToSave,
								genericObject -> genericObject.withAddedAttributes(PandoraIndexConstants.ATTRIBUTE, originAttribute));
						storeRouteManipulator.createWithAdjustment(
								StoreRoute.builder()
										.distance(0)
										.slug("%s-route".formatted(storeConfigurationToSave.getSlug()))
										.storeConfigurationPredicate(Predicates.equalTo(
												StoreConfiguration.SLUG_ATTRIBUTE_NAME,
												storeConfigurationToSave.getSlug()))
										.testPredicate(objectModelReferencePredicate)
										.build(),
								genericObject -> genericObject.withAddedAttributes(PandoraIndexConstants.ATTRIBUTE, originAttribute));
					}
				}
			}
		}
	}

	@Nullable
	public static MongoDbStoreSettings resolveMongoDbSettings(AdmincowStoreConfiguration storeConfiguration) {
		ConnectionString connectionString = new ConnectionString(storeConfiguration.getUrl());
		//
		return connectionString.getDatabase() != null
				? MongoDbStoreSettings.builder()
				.uri(storeConfiguration.getUrl())
				.build()
				: null;
	}
}
